import React, { useState } from "react";
import doordeckSdk from "@doordeck/javascript-doordeck-sdk";
import "./App.css";

function App() {
  const [bearerToken, setBearerToken] = useState("");
  const [verificationToken, setVerificationToken] = useState("");

  function init() {
    doordeckSdk.doordeckInit(bearerToken).then(
      (response) => {
        // doordeck successfully initialised
        console.log("response", response);
        // doordeckSdk.device.unlock("ad8fb900-4def-11e8-9370-170748b9fca8");
      },
      (error) => {
        if (error.state === "verify") {
          // show verification screen
        } else {
          // catch error
        }
      }
    );
  }

  function verify(code) {
    doordeckSdk.verifyCode(code.toString());
  }

  function openLock() {
    doordeckSdk.device.unlock("ad8fb900-4def-11e8-9370-170748b9fca8");
  }

  return (
    <div className="App">
      1. Click on the button:
      <br />
      <iframe
        title="doordeck simulation door"
        src="https://demo.doordeck.com/assets/"
        height="400"
        width="600"
      />
      <hr />
      2. Enter your bearer token:
      <br />
      <input
        type="text"
        value={bearerToken}
        onChange={(e) => setBearerToken(e.target.value)}
      ></input>
      <hr />
      3. Press this button to init the sdk:
      <br />
      <button onClick={init}>click me</button>
      <br />
      3b. (optionally) Enter the verification code if you received one:
      <br />
      <input
        type="text"
        value={verificationToken}
        onChange={(e) => setVerificationToken(e.target.value)}
      />
      <button onClick={verify}>ok</button>
      <hr />
      4. Press this button to open the demo door (watch animation in step 1)
      <br />
      <button onClick={openLock}>click me</button>
    </div>
  );
}

export default App;
